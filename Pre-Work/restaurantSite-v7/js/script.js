window.onload = function () {
	eventHandlers();
};

function eventHandlers() {
	var btnSubmit = document.getElementById("btnSubmit");
	var name = document.getElementById("name");
	var email = document.getElementById("email");
	var phone = document.getElementById("phone");
	var drpDwnOther = document.getElementById("drpDwnOther");
	var addlInfo = document.getElementById("info");
	var checkArray = [document.getElementById("mon"), 
					document.getElementById("tues"), 
					document.getElementById("wed"), 
					document.getElementById("thu"), 
					document.getElementById("fri")
					];
	var dayChecked = false;

	drpDwnOther.onclick = function() {
		if (addlInfo.value == "") {
			alert("Please provide us Additional Information");
		}
	};

	btnSubmit.onclick = function() {

		if (name.value == "") {
			alert("Please enter a name.");
		}

		if (email.value == "" && phone.value == "") {
			alert("Please enter a valid email address or phone number.");
		}

		for (i=0; i<checkArray.length; i++) {
			console.log(checkArray[i]);
			console.log("daychecked: "+dayChecked);
			if (checkArray[i].checked) {
				dayChecked=true;
			}
		}

		if (!dayChecked) {
			alert("Please select at least one day on which you may be contacted.")
		}

	}
};



