//prepare page to start play
document.getElementById("tblResults").style.visibility = "hidden";

var inputBet = document.getElementById("bet");
inputBet.onfocus = function() {
	inputBet.value = "";
};


//do stuff when Play button is clicked
var playBtn = document.getElementById("btnSubmit");

playBtn.onclick = function() {
	var money = inputBet.value;
	money = parseFloat(money.replace("$",""));

//check for valid entry
	if (!isNaN(money)) {
		var rollCount = 0;
		var maxMoney=inputBet.value;
		maxMoney = parseFloat(maxMoney.replace("$",""));
		var maxMoneyRolls = rollCount;

//roll the dice and do the money
		while (money>0) {
			var dieOne = Math.floor(Math.random()*6) +1;
			var dieTwo = Math.floor(Math.random()*6) +1;
			rollCount++;
			rollSum = dieOne + dieTwo;

			if (rollSum == 7) {
				money += 4;

				if (money>maxMoney) {
					maxMoney=money;
					maxMoneyRolls=rollCount;
				}
			}

			else {
				money -= 1;
			}
		}

//add values to tbl and show
		document.getElementById("tblBet").innerHTML = inputBet.value;
		document.getElementById("tblwon").innerHTML= maxMoney;
		document.getElementById("tblRolls").innerHTML = maxMoneyRolls;
		document.getElementById("tblResults").style.visibility = "visible";


//clear played values for another game
		alert("Please enter a new value to play again!");

		document.getElementById("tblBet").innerHTML = "";
		document.getElementById("tblwon").innerHTML= "";
		document.getElementById("tblRolls").innerHTML = "";
		document.getElementById("bet").value = "$0.00";
		document.getElementById("tblResults").style.visibility = "hidden";
	}

//if invalid entry
	else {
		alert("Please enter a valid dollar amount.");
		document.getElementById("bet").value = "$0.00";
		document.getElementById("tblResults").style.visibility = "hidden";
	}

};







